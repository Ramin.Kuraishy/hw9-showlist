"use strict";
const arr = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];
const parent = document.body;
const renderArr = (arr, parent) => {
  arr.forEach(element => {
    if (Array.isArray(element)) {
      element.forEach(element2 => {
        parent.insertAdjacentHTML(
          "beforeend",
          `<ul> <li>${element2}</li> </ul>`
        );
      });
    } else {
      parent.insertAdjacentHTML("beforeend", `<li>${element}</li>`);
    }
  });
};
renderArr(arr, parent);
